import com.google.common.collect.Maps;

import java.util.Map;


public class StatsModule {

    private static final Map<String, Long> statistics = Maps.newHashMap();

    public static void incrementCounter(String instruction) {
        statistics.put(instruction, statistics.getOrDefault(instruction, 0L) + 1L);
    }

    public static void createShutdownHook() {
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                for (Map.Entry<String, Long> entry : statistics.entrySet()) {
                    String key = entry.getKey();
                    Long value = entry.getValue();
                    if (value >= 5) {
                        System.out.println(key.toUpperCase() + "    " + value);
                    }
                }
            }
        });
    }
}