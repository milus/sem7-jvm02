import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import org.apache.bcel.Constants;
import org.apache.bcel.Repository;
import org.apache.bcel.classfile.JavaClass;
import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.*;

import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;
import java.util.Objects;
import java.util.Optional;

public class Transform {

    private static final String STATISTICS_UTIL_CLASS_NAME = StatsModule.class.getCanonicalName();
    private static final String INCREMENT_INSTR_COUNTER_METHOD_NAME = "incrementCounter";
    private static final String CREATE_SHUTDOWN_HOOK_METHOD_NAME = "createShutdownHook";

    // init methods
    private static final String CLINIT_METHOD_NAME = "<clinit>";
    private static final String INIT_METHOD_NAME = "<init>";

    private static final String MAIN_METHOD_NAME = "main";

    private static final String IGNORED_FIRST_LETTER = "m";

    private final JavaClass clazz;
    private final ClassGen classGenerator;
    private final ConstantPoolGen constantPoolGenerator;

    public Transform(String className) throws ClassNotFoundException {
        this.clazz = Repository.lookupClass(className);
        this.classGenerator = new ClassGen(clazz);
        this.constantPoolGenerator = classGenerator.getConstantPool();
    }

    public static void main(String[] args) throws Exception {
        Optional<String> className = loadClassFrom(args);

        if (!className.isPresent()) {
            exitOnError("Cannot load class file. Please verify that you passed correct file");
        }

        new Transform(className.get()).start();
    }

    private static Optional<String> loadClassFrom(String[] args) {
        if (args.length != 1) {
            return Optional.empty();
        }

        return Optional.of(args[0]);
    }

    private static void exitOnError(String errorMsg) {
        Objects.requireNonNull(errorMsg);

        System.err.println(errorMsg);
        System.exit(1);
    }

    public void start() throws IOException, ClassNotFoundException {
        for (Method method : clazz.getMethods()) {
            injectStatsGenerator(method);
        }
        classGenerator.getJavaClass().dump(Repository.lookupClassFile(classGenerator.getClassName()).getPath());
    }

    private void injectStatsGenerator(Method method) {
        if (isInit(method)) {
            return;
        }

        MethodGen methodGenerator = new MethodGen(method, classGenerator.getClassName(), constantPoolGenerator);
        InstructionFactory factory = new InstructionFactory(classGenerator);
        InstructionList instructions = methodGenerator.getInstructionList();

        if (isMainMethod(method)) {
            injectShutdownHook(instructions, factory);
        }

        Collection<InstructionHandle> handles = Collections2.filter(Lists.newArrayList(((Iterator<InstructionHandle>) instructions.iterator())), new Predicate<InstructionHandle>() {
            @Override
            public boolean apply(InstructionHandle instructionHandle) {
                return !isExternal(instructionHandle) && !startsWithIgnoredLetter(instructionHandle);
            }
        });

        for (InstructionHandle handle : handles) {
            addIncrementCounterCall(instructions, factory, handle);
        }

        methodGenerator.setMaxStack();
        methodGenerator.setMaxLocals();
        classGenerator.replaceMethod(method, methodGenerator.getMethod());
    }

    private boolean isInit(Method method) {
        return method.getName().equals(INIT_METHOD_NAME) || method.getName().equals(CLINIT_METHOD_NAME);
    }

    private boolean isMainMethod(Method method) {
        Type[] methodArgumentTypes = method.getArgumentTypes();
        return method.getName().equals(MAIN_METHOD_NAME)
                && method.getReturnType().equals(Type.VOID)
                && methodArgumentTypes.length == 1
                && methodArgumentTypes[0].equals(Type.getType(String[].class))
                && method.isPublic()
                && method.isStatic();
    }

    private void injectShutdownHook(InstructionList instructionList, InstructionFactory factory) {
        instructionList.insert(factory.createInvoke(STATISTICS_UTIL_CLASS_NAME,
                CREATE_SHUTDOWN_HOOK_METHOD_NAME, Type.VOID, Type.NO_ARGS, Constants.INVOKESTATIC));
    }

    private boolean isExternal(InstructionHandle instructionHandle) {
        Instruction instruction = instructionHandle.getInstruction();

        if (!(instruction instanceof InvokeInstruction)) {
            return false;
        }

        final InvokeInstruction invokeInstruction = (InvokeInstruction) instruction;

        return Iterables.all(Lists.newArrayList(clazz.getMethods()), new Predicate<Method>() {
            @Override
            public boolean apply(Method method) {
                return !isDeclaredCall(invokeInstruction, method);
            }
        });

    }

    private boolean isDeclaredCall(InvokeInstruction invokeInstruction, Method method) {
        return classGenerator.getClassName().equals(invokeInstruction.getReferenceType(constantPoolGenerator).toString())
                && method.getName().equals(invokeInstruction.getMethodName(constantPoolGenerator))
                && method.getSignature().equals(invokeInstruction.getSignature(constantPoolGenerator));
    }

    private boolean startsWithIgnoredLetter(InstructionHandle instructionHandle) {
        Instruction instruction = instructionHandle.getInstruction();

        if (!(instruction instanceof InvokeInstruction)) {
            return false;
        }

        return ((InvokeInstruction) instruction).getMethodName(constantPoolGenerator).startsWith(IGNORED_FIRST_LETTER);
    }

    private void addIncrementCounterCall(InstructionList instructionList, InstructionFactory factory, InstructionHandle instructionHandle) {
        instructionList.insert(instructionHandle, factory.createConstant(instructionHandle.getInstruction().getName()));
        instructionList.insert(instructionHandle, factory.createInvoke(STATISTICS_UTIL_CLASS_NAME, INCREMENT_INSTR_COUNTER_METHOD_NAME,
                Type.VOID, new Type[]{Type.STRING}, Constants.INVOKESTATIC));
    }
}